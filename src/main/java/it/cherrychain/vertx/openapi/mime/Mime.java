package it.cherrychain.vertx.openapi.mime;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.util.Json;
import io.swagger.util.Yaml;
import it.cherrychain.vertx.openapi.OpenApiSpecException;

public interface Mime {
  default <T> String asJson(final T value) {
    return Json.pretty(value);
  }

  default <T> String asYaml(final T value) {
    try {
      return Yaml.pretty().writeValueAsString(value);
    } catch (JsonProcessingException e) {
      throw new OpenApiSpecException("Can't serialize to application/yaml.");
    }
  }
}
