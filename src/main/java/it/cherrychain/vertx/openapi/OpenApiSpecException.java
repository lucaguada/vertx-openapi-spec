package it.cherrychain.vertx.openapi;

public final class OpenApiSpecException extends RuntimeException {
  public OpenApiSpecException() {
    super();
  }

  public OpenApiSpecException(String message) {
    super(message);
  }

  public OpenApiSpecException(String message, Throwable cause) {
    super(message, cause);
  }

  public OpenApiSpecException(Throwable cause) {
    super(cause);
  }

  protected OpenApiSpecException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
