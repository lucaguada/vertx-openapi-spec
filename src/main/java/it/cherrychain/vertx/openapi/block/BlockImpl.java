package it.cherrychain.vertx.openapi.block;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

final class BlockImpl<B> implements Block<B> {
  private final B block;

  BlockImpl(final B block) {
    this.block = block;
  }

  @Override
  public <R> Block<R> map(Function<? super B, ? extends R> transform) {
    final var any = requireNonNull(transform, "Transform can't be null.").apply(block);
    return nonNull(any) ? new BlockImpl<>(any) : new EmptyBlock<>();
  }

  @Override
  public Block<B> peek(Consumer<? super B> peek) {
    requireNonNull(peek, "Peek can't be null.").accept(block);
    return this;
  }

  @Override
  public Block<B> then(Runnable then) {
    requireNonNull(then, "Then can't be null.").run();
    return this;
  }

  @Override
  public Block<B> filter(Predicate<? super B> filter) {
    return requireNonNull(filter, "Filter can't be null.").test(block)
      ? this
      : new EmptyBlock<>();
  }

  @Override
  public B get() {
    return block;
  }
}
