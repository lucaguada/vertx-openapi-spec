package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Paths;

final class PathsSpec implements OpenApiProperty<Paths> {
  private final PathsProperty<?>[] properties;

  PathsSpec(final PathsProperty<?>[] properties) {
    this.properties = properties;
  }

  @Override
  public final Paths apply(final OpenAPI openAPI) {
    final var paths = new Paths();
    for (final var property : properties) property.apply(paths);
    openAPI.setPaths(paths);
    return paths;
  }
}
