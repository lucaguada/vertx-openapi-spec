package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import it.cherrychain.vertx.openapi.OpenApiSpecException;

final class OperationSpec implements PathItemProperty<Operation> {
  private final RouteMethods methods;
  private final OperationProperty<?>[] properties;

  OperationSpec(final RouteMethods methods, final OperationProperty<?>[] properties) {
    this.methods = methods;
    this.properties = properties;
  }

  @Override
  public final Operation apply(final PathItem pathItem) {
    final var operation = new Operation();
    for (final var property : properties) property.apply(operation);
    for (final var method : methods) {
      switch (method) {
        case GET:     pathItem.setGet(operation);     break;
        case POST:    pathItem.setPost(operation);    break;

        case PUT:     pathItem.setPut(operation);     break;
        case PATCH:   pathItem.setPatch(operation);   break;

        case DELETE:  pathItem.setDelete(operation);  break;

        case HEAD:    pathItem.setHead(operation);    break;
        case OPTIONS: pathItem.setOptions(operation); break;

        default: throw new OpenApiSpecException("Can't set path-item on unknown http-method.");
      }
    }
    return operation;
  }
}

