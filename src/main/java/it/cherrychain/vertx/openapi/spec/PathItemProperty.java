package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;

import java.util.function.Function;

import static java.util.Arrays.copyOf;

public interface PathItemProperty<T> extends Function<PathItem, T> {
  static PathItemProperty<Operation> operationSpec(final RouteMethods methods, final OperationProperty... properties) {
    return new OperationSpec(methods, copyOf(properties, properties.length));
  }
}
