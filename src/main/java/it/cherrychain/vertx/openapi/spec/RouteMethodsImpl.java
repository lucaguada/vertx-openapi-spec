package it.cherrychain.vertx.openapi.spec;

import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.impl.RouteImpl;
import it.cherrychain.vertx.openapi.OpenApiSpecException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Set;
import java.util.stream.Stream;

final class RouteMethodsImpl implements RouteMethods {
  private static final Logger log = LoggerFactory.getLogger(RouteMethodsImpl.class);

  private final Route route;

  RouteMethodsImpl(final Route route) {
    this.route = route;
  }

  @Override
  public final Iterator<HttpMethod> iterator() {
    return Stream.of(routeFields())
      .peek(it -> it.setAccessible(true))
      .filter(it -> it.getName().equals("methods"))
      .flatMap(it -> httpMethods(route, it))
      .iterator();
  }

  private Field[] routeFields() {
    return ((RouteImpl) route).getClass().getDeclaredFields();
  }

  @SuppressWarnings("unchecked")
  private Stream<HttpMethod> httpMethods(Route route, Field it) {
    try {
      return ((Set<HttpMethod>) it.get(route)).stream();
    } catch (IllegalAccessException e) {
      log.error("Can't find backing field `methods` in RouteImpl.");
      throw new OpenApiSpecException(e);
    }
  }
}
