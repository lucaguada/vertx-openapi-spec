package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.Paths;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.oas.models.tags.Tag;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static java.util.Arrays.copyOf;

public interface OpenApiProperty<T> extends Function<OpenAPI, T> {
  static OpenApiProperty<List<Server>> serversSpec() {
    return api -> {
      final var servers = new ArrayList<Server>();
      api.setServers(servers);
      return servers;
    };
  }

  static OpenApiProperty<List<Tag>> tagsSpec() {
    return api -> {
      final var tags = new ArrayList<Tag>();
      api.setTags(tags);
      return tags;
    };
  }

  static OpenApiProperty<Info> infoSpec(final String title, final String description) {
    return api -> {
      final var info = new Info();
      info.setTitle(title);
      info.setDescription(description);
      api.setInfo(info);
      return info;
    };
  }

  static OpenApiProperty<Paths> pathsSpec(final PathsProperty<?>... properties) {
    return new PathsSpec(copyOf(properties, properties.length));
  }
}
