package it.cherrychain.vertx.openapi.spec;

import java.util.function.Supplier;

import static java.util.Arrays.copyOf;

public interface PathSpec extends Supplier<String> {
  static PathSpec pathSpec(final String path) {
    return pathSpec(path.split("/"));
  }

  static PathSpec pathSpec(final String... splittedPath) {
    return new PathSpecImpl(copyOf(splittedPath, splittedPath.length));
  }
}
