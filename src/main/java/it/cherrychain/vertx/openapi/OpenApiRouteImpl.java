package it.cherrychain.vertx.openapi;

import io.vertx.ext.web.Router;
import it.cherrychain.vertx.openapi.mime.Mime;
import it.cherrychain.vertx.openapi.spec.OpenApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static it.cherrychain.vertx.openapi.block.Block.$;

final class OpenApiRouteImpl implements OpenApiRoute, Mime {
  private static final Logger log = LoggerFactory.getLogger(OpenApiRouteImpl.class);

  private static final String CONTENT_TYPE = "content-type";
  private static final String JSON = "application/json; charset=UTF-8";
  private static final String YAML = "application/yaml; charset=UTF-8";

  private final Router router;
  private final OpenApi openApi;

  OpenApiRouteImpl(final Router router, final OpenApi openApi) {
    this.router = router;
    this.openApi = openApi;
  }

  @Override
  public final void on(final String path) {
    router.get(path)
      .consumes(JSON)
      .consumes(YAML)
      .produces(JSON)
      .produces(YAML)
      .handler(it ->
        it.response()
          .putHeader(CONTENT_TYPE, it.request().getHeader(CONTENT_TYPE))
          .end(
            $(it.request().getHeader(CONTENT_TYPE))
              .peek(type -> log.info("Type {}", type))
              .get()
              .contains(JSON)
              ? $(asJson(openApi.get())).peek(log::info).get()
              : $(asYaml(openApi.get())).peek(log::info).get()
          )
      );
  }
}
