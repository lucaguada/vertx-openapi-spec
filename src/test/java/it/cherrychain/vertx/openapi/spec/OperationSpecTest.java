package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.PathItem;
import io.vertx.ext.web.Route;
import org.junit.jupiter.api.Test;

import static io.vertx.core.Vertx.vertx;
import static io.vertx.ext.web.Router.router;
import static it.cherrychain.vertx.openapi.spec.OperationProperty.parametersSpec;
import static it.cherrychain.vertx.openapi.spec.PathItemProperty.operationSpec;
import static it.cherrychain.vertx.openapi.spec.RouteMethods.routeMethods;
import static org.assertj.core.api.Assertions.assertThat;

class OperationSpecTest {
  private final Route route = router(vertx()).get("/api/:id").handler(it -> it.response().end());
  private final PathItem pathItem = new PathItem();

  @Test
  void shouldGetOperationWithParams() {
    final var spec = operationSpec(
      routeMethods(route),
      parametersSpec("/api/:any/resource/:id")
    );

    assertThat(spec.apply(pathItem)).isNotNull();
    assertThat(spec.apply(pathItem).getParameters().get(0).getName()).isEqualTo("any");
    assertThat(spec.apply(pathItem).getParameters().get(1).getName()).isEqualTo("id");
  }
}